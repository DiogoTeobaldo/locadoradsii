package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import util.ConexaoUtil;
import entity.Modelo_Veiculo;

public class ModeloDAO {
	
	Connection conection;
	PreparedStatement statement;
	
	public Modelo_Veiculo findModelo(String idModelo) {
		String sqlModelo = "Select * from \"Modelo\" where \"idModelo\" =?"; 
		Modelo_Veiculo modelo = null;
		try { 
			conection = ConexaoUtil.getConnection();
			statement = conection.prepareStatement(sqlModelo);
			statement.setString(1, idModelo); 
			ResultSet result = statement.executeQuery();
			if (result.next()) {
				modelo = new Modelo_Veiculo(result.getString("nome"), result.getString("marca"), result.getInt("ano"));
			} 
		} catch (SQLException e) { 
			JOptionPane.showMessageDialog(null, e.getMessage());
		} finally { 
			try { 
				if (conection != null) 
					conection.close();
				if (statement != null) 
					statement.close();
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			} 
		} 
		return modelo; 
	}

}
