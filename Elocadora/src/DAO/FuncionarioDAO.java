package DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import util.ConexaoUtil;
import entity.Funcionario;

public class FuncionarioDAO {
	Connection connection;
	PreparedStatement statement;
	public void addFuncionario(Funcionario funcionario) {
		String sql = "Insert into \"Funcionarios\" (cpf, nome, telefone, email, \"dataNascimento\", salario, cargo, \"dataContratacao\", turno, senha, idfuncionario) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		try {
			connection = ConexaoUtil.getConnection();
			statement = connection.prepareStatement(sql);
			statement.setString(1,funcionario.getCpf());
			statement.setString(2,funcionario.getNome());
			statement.setString(3,funcionario.getTelefone());
			statement.setString(4,funcionario.getEmail());
			statement.setDate(5,converteData(funcionario.getData()));
			statement.setFloat(6,funcionario.getSalario());
			statement.setString(7,funcionario.getCargo());
			statement.setDate(8,converteData(funcionario.getData_cont()));
			statement.setString(9,funcionario.getTurno());
			statement.setString(10, funcionario.getSenha());
			statement.setString(11, funcionario.getIdFuncionario());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			try {
				if (statement != null) statement.close();
				if (connection != null) connection.close();
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		}
	}	

	public Funcionario getFuncionario(String cpf) {
		String sql = "Select * from \"Funcionarios\" where cpf=?";	
		Funcionario funcionario= null;
		ResultSet result = null;
		try {
			connection = ConexaoUtil.getConnection();
			statement = connection.prepareStatement(sql);
			statement.setString(1,cpf);
			result = statement.executeQuery();
			while(result.next()) {			
				funcionario = new Funcionario();
				funcionario.setCargo(result.getString("cargo"));
				funcionario.setTurno(result.getString("turno"));
				funcionario.setSalario(result.getFloat("salario"));
				funcionario.setData_cont(result.getString("dataContratacao"));
				funcionario.setNome(result.getString("nome"));
				funcionario.setCpf(result.getString("cpf"));
				funcionario.setTelefone(result.getString("telefone"));
				funcionario.setEmail(result.getString("email"));
				funcionario.setData(result.getString("dataNascimento"));
				funcionario.setSenha(result.getString("senha"));
			}
			result.close();
			statement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		finally{
			try {
				if (result != null) result.close();
				if (statement != null) statement.close();
				if (connection != null) connection.close();
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		}
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		String sql = "Update \"Funcionarios\" set nome=?,telefone=?,email=?,\"dataNascimento\"=?,salario=?,cargo=?,"
				+ "\"dataContratacao\"=?,turno=?, idfuncionario=? where cpf=?";
		try {
			connection = ConexaoUtil.getConnection();
			statement = connection.prepareStatement(sql);
			statement.setString(1,funcionario.getNome());
			statement.setString(2,funcionario.getTelefone());
			statement.setString(3,funcionario.getEmail());
			statement.setDate(4,converteData(funcionario.getData()));
			statement.setFloat(5,funcionario.getSalario());
			statement.setString(6,funcionario.getCargo());
			statement.setDate(7,converteData(funcionario.getData_cont()));
			statement.setString(8,funcionario.getTurno());
			statement.setString(9, funcionario.getIdFuncionario());
			statement.setString(10, funcionario.getCpf());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			try {
				if (statement != null) statement.close();
				if (connection != null) connection.close();
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		}
	}

	private Date converteData(String data) {
		return Date.valueOf(data);		
	}
}
