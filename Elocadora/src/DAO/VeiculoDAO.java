package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import util.ConexaoUtil;
import entity.Veiculo;

public class VeiculoDAO {
	Connection conection;
	PreparedStatement statement;

	public void addVeiculo(Veiculo veiculo){
		String sql = "Insert into veiculos (idveiculo,idmodelo,combustivel,status,"
				+ "preco,cor,kilometragem,idfuncionario) values (?,?,?,?,?,?,?,?)";
		try {
			conection = ConexaoUtil.getConnection();
			statement = conection.prepareStatement(sql);
			statement.setString(1, veiculo.getIdVeiculo());
			statement.setString(2, veiculo.getModelo());
			statement.setString(3, veiculo.getCombustivel());
			statement.setString(4, veiculo.getStatus());
			statement.setFloat(5, veiculo.getPreco());
			statement.setString(6, veiculo.getCor());
			statement.setFloat(7, veiculo.getKilometragem());
			statement.setString(8, veiculo.getIdFuncionario());
			statement.executeUpdate();
		}catch(SQLException e){
			JOptionPane.showMessageDialog(null, e.getMessage()); 
		}
		finally{
			try {
				if (conection != null)
					conection.close();
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		}
	}

	public Veiculo findVeiculoPlaca(String placa) {
		String sql = "Select * from veiculos where idveiculo =?";
		Veiculo veiculo = null;
		try {
			conection = ConexaoUtil.getConnection();
			statement = conection.prepareStatement(sql);
			statement.setString(1, placa);
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				veiculo = new Veiculo();
				veiculo.setIdVeiculo(result.getString("idveiculo"));
				veiculo.setCombustivel(result.getString("combustivel"));
				veiculo.setStatus(result.getString("status"));
				veiculo.setKilometragem(result.getFloat("kilometragem"));
				veiculo.setPreco(result.getFloat("preco"));
				veiculo.setCor(result.getString("cor"));
				veiculo.setModelo(result.getString("idmodelo"));
			}

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage()); 
		}
		finally{
			try {
				if (conection != null)
					conection.close();
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		}
		return veiculo;
	}

	public ArrayList<Veiculo> findVeiculoNome(String nome) {
		String sql = "Select v.idveiculo, v.combustivel, v.status, v.kilometragem, v.preco, v.cor, v.idmodelo "
				+ "from veiculos as v join \"Modelo\" as m on(v.idmodelo = m.\"idModelo\") "
				+ "where m.nome =? "
				+ "order by status";
		ArrayList<Veiculo> veiculos = null;
		Veiculo veiculo = null;
		int contador = 0;
		try {
			conection = ConexaoUtil.getConnection();
			statement = conection.prepareStatement(sql);
			statement.setString(1, nome);
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				if(contador == 0) {
					veiculos = new ArrayList<Veiculo>();
				}
				veiculo = new Veiculo();
				veiculo.setIdVeiculo(result.getString("idveiculo"));
				veiculo.setCombustivel(result.getString("combustivel"));
				veiculo.setStatus(result.getString("status"));
				veiculo.setKilometragem(result.getFloat("kilometragem"));
				veiculo.setPreco(result.getFloat("preco"));
				veiculo.setCor(result.getString("cor"));
				veiculo.setModelo(result.getString("idmodelo"));
				veiculos.add(veiculo);
				contador++;
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage()); 
		}
		finally{
			try {
				if (conection != null)
					conection.close();
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		}
		return veiculos;
	}

	public ArrayList<Veiculo> findVeiculoMarca(String marca) {
		String sql = "Select v.idveiculo, v.combustivel, v.status, v.kilometragem, v.preco, v.cor, v.idmodelo "
				+ "from veiculos as v join \"Modelo\" as m on(v.idmodelo = m.\"idModelo\") "
				+ "where m.marca =? "
				+ "order by status";
		ArrayList<Veiculo> veiculos = null;
		Veiculo veiculo = null;
		int contador = 0;
		try {
			conection = ConexaoUtil.getConnection();
			statement = conection.prepareStatement(sql);
			statement.setString(1, marca);
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				if(contador == 0) {
					veiculos = new ArrayList<Veiculo>();
				}
				veiculo = new Veiculo();
				veiculo.setIdVeiculo(result.getString("idveiculo"));
				veiculo.setCombustivel(result.getString("combustivel"));
				veiculo.setStatus(result.getString("status"));
				veiculo.setKilometragem(result.getFloat("kilometragem"));
				veiculo.setPreco(result.getFloat("preco"));
				veiculo.setCor(result.getString("cor"));
				veiculo.setModelo(result.getString("idmodelo"));
				veiculos.add(veiculo);
				contador++;
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage()); 
		}
		finally{
			try {
				if (conection != null)
					conection.close();
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		}
		return veiculos;
	}

	public void updateVeiculo(Veiculo veiculo){
		String sql = "Update veiculos set combustivel=?, "
				+ "status=?, preco=?, cor=?, kilometragem=?, idfuncionario=? where idveiculo =?";
		try {
			conection = ConexaoUtil.getConnection();
			statement = conection.prepareStatement(sql);
			statement.setString(1, veiculo.getCombustivel());
			statement.setString(2, veiculo.getStatus());
			statement.setFloat(3, veiculo.getPreco());
			statement.setString(4, veiculo.getCor());
			statement.setFloat(5, veiculo.getKilometragem());
			statement.setString(6, veiculo.getIdFuncionario());
			statement.setString(7, veiculo.getIdVeiculo());
			statement.executeUpdate();
		}catch(SQLException e){
			JOptionPane.showMessageDialog(null, e.getMessage()); 
		}
		finally{
			try {
				if (conection != null)
					conection.close();
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		}
	}

	public void deleteVeiculo(String idveiculo){
		String sql = "Delete From veiculos where idveiculo =?";
		try {
			conection = ConexaoUtil.getConnection();
			statement = conection.prepareStatement(sql);
			statement.setString(1, idveiculo);
			statement.executeUpdate();
		}catch(SQLException e){
			JOptionPane.showMessageDialog(null, e.getMessage()); 
		}
		finally{
			try {
				if (conection != null)
					conection.close();
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		}
	}
}
