package DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;

import javax.swing.JOptionPane;

import util.ConexaoUtil;
import entity.Reserva;

public class ReservaDAO {
	
	public Reserva findReserva(String idReserva) {
		Connection conection = null;
		PreparedStatement statement = null;
		String sql = "Select * from Reservas where idreserva =?";
		Reserva reserva = null;
		try {
			conection = ConexaoUtil.getConnection();
			statement = conection.prepareStatement(sql);
			statement.setString(1, idReserva);
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				reserva = new Reserva();
				reserva.setIdReserva(result.getString("idreserva"));
				reserva.setidCliente(result.getString("idCliente"));
				reserva.setHora(result.getString("hora"));
				reserva.setData(result.getString("data"));
			}
				
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage()); 
		}
		finally{
			try {
				if (conection != null)
					conection.close();
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		}
		return reserva;
	}
	
	@SuppressWarnings("resource")
	public void addReserva(Reserva reserva){
		Connection conection = null;
		PreparedStatement statement = null;
		String sql = "Insert into reservas (idCliente, data, hora, idveiculo) "
				+ "values(?, ?, ?, ?)";
		String sqlveiculo = "Update veiculos set status = 'Reservado'" +
				" where idveiculo =?";
		try {
			conection = ConexaoUtil.getConnection();
			statement = conection.prepareStatement(sql);
			statement.setString(1, reserva.getidCliente());
		
			statement.setDate(2, converteData(reserva.getData()));
			statement.setTime(3, Time.valueOf(reserva.getHora() + ":00"));
			statement.setString(4, reserva.getidVeiculo());
			statement.executeUpdate();
			
			statement = conection.prepareStatement(sqlveiculo);
			statement.setString(1, reserva.getidVeiculo());
			statement.executeUpdate();
		}catch(SQLException e){
			JOptionPane.showMessageDialog(null, e.getMessage()); 
		}
		finally{
			try {
				if (conection != null)
					conection.close();
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		}
	}
	
	private Date converteData(String data){
		String dataformatada = new String();
		dataformatada =  data.substring(6, 10);
		dataformatada += data.substring(2, 6);
		dataformatada += data.substring(0, 2);
		return Date.valueOf(dataformatada);
		
	}
	
//	private Time converteHora(String hora){
//		
//	}
}