package views;
import java.text.ParseException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.text.MaskFormatter;

import controller.VeiculoController;
import entity.LoginFuncionario;
import entity.Veiculo;

@SuppressWarnings("serial")
public class CadastrarVeiculo extends JFrame {

	public CadastrarVeiculo() {
        initComponents();
    }
	
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void initComponents() {
    	veiculoController = new VeiculoController();
        panel_top = new JPanel();
        label_logo1 = new JLabel();
        label_logo2 = new JLabel();
        panel_principal = new JPanel();
        panel_label = new JPanel();
        label_Placa = new JLabel();
        label_Cor = new JLabel();
        label_Modelo = new JLabel();
        label_km = new JLabel();
        label_Combustivel = new JLabel();
        label_Preco = new JLabel();
        panel_text = new JPanel();
        try {
			tf_Placa = new JFormattedTextField(new MaskFormatter("UUU-####"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
        tf_Cor = new JTextField();
        tf_km = new JTextField();
        comboBox_Combustivel = new JComboBox();
        tf_Preco = new JTextField();
        tf_Modelo = new JTextField();
        bt_cadastrar = new JButton();
        bt_cancelar = new JButton();


        label_logo1.setFont(new java.awt.Font("Lucida Grande", 1, 36)); // NOI18N
        label_logo1.setText("Locadora de Veiculos");

        label_logo2.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        label_logo2.setText("Cadastrar Veiculo");

        GroupLayout panel_topLayout = new GroupLayout(panel_top);
        panel_top.setLayout(panel_topLayout);
        panel_topLayout.setHorizontalGroup(
            panel_topLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_topLayout.createSequentialGroup()
                .addGroup(panel_topLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(panel_topLayout.createSequentialGroup()
                        .addGap(98, 98, 98)
                        .addComponent(label_logo1))
                    .addGroup(panel_topLayout.createSequentialGroup()
                        .addGap(214, 214, 214)
                        .addComponent(label_logo2, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(97, Short.MAX_VALUE))
        );
        panel_topLayout.setVerticalGroup(
            panel_topLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_topLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(label_logo1)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
                .addComponent(label_logo2)
                .addContainerGap())
        );
        label_Placa.setText("Placa: ");
        
        label_Cor.setText("Cor:");
        
        label_Modelo.setText("Modelo: ");

        label_km.setText("Kilometragem:");

        label_Combustivel.setText("Combustivel:");

        label_Preco.setText("Preco:");

        GroupLayout panel_labelLayout = new GroupLayout(panel_label);
        panel_label.setLayout(panel_labelLayout);
        panel_labelLayout.setHorizontalGroup(
            panel_labelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_labelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_labelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(panel_labelLayout.createSequentialGroup()
                        .addGroup(panel_labelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        	.addComponent(label_Placa)
                        	.addComponent(label_Modelo)
                        	.addComponent(label_Cor)
                            .addComponent(label_km)
                            .addComponent(label_Preco)
                            .addComponent(label_Combustivel))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panel_labelLayout.setVerticalGroup(
            panel_labelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_labelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(label_Placa)
                .addGap(18, 18, 18)
                .addComponent(label_Modelo)
                .addGap(18, 18, 18)
                .addComponent(label_Cor)
                .addGap(18, 18, 18)
                .addComponent(label_km)
                .addGap(18, 18, 18)
                .addComponent(label_Preco)
                .addGap(18, 18, 18)
                .addComponent(label_Combustivel)
                .addGap(18, 18, 18))
        );

        comboBox_Combustivel.setModel(new DefaultComboBoxModel(new String[] { "Gasolina", "Gas", "Alcool", " " }));

        bt_cadastrar.setText("Cadastrar");
        bt_cadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_cadastrarActionPerformed(evt);
            }
        });
        bt_cancelar.setText("Cancelar");
        bt_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_cancelarActionPerformed(evt);
            }
        });

        GroupLayout panel_textLayout = new GroupLayout(panel_text);
        panel_text.setLayout(panel_textLayout);
        panel_textLayout.setHorizontalGroup(
            panel_textLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_textLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_textLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(panel_textLayout.createSequentialGroup()
                        .addGroup(panel_textLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                           .addGroup(panel_textLayout.createSequentialGroup()
                                .addGroup(panel_textLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addGroup(panel_textLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                    	.addComponent(tf_Placa)
                                    	.addComponent(tf_Modelo)
                                    	.addComponent(tf_Cor)
                                        .addComponent(tf_km)
                                        .addComponent(tf_Preco)
                                        .addComponent(comboBox_Combustivel, 0, 140, Short.MAX_VALUE)))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(GroupLayout.Alignment.TRAILING, panel_textLayout.createSequentialGroup()
                        .addGap(0, 209, Short.MAX_VALUE)
                        .addComponent(bt_cadastrar)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt_cancelar))))
        );
        panel_textLayout.setVerticalGroup(
            panel_textLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_textLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tf_Placa, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tf_Modelo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tf_Cor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tf_km, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tf_Preco, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboBox_Combustivel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addGroup(panel_textLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(bt_cadastrar)
                    .addComponent(bt_cancelar)))
        );

        GroupLayout panel_principalLayout = new GroupLayout(panel_principal);
        panel_principal.setLayout(panel_principalLayout);
        panel_principalLayout.setHorizontalGroup(
            panel_principalLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_principalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel_label, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(panel_text, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        panel_principalLayout.setVerticalGroup(
            panel_principalLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, panel_principalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_principalLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addComponent(panel_text, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panel_label, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addComponent(panel_principal, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panel_top, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel_top, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel_principal, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void bt_cadastrarActionPerformed(java.awt.event.ActionEvent evt) {
    	if(!veiculoController.validaPlaca(tf_Placa.getText())) {
    		JOptionPane.showMessageDialog(null, "Placa invalida!");
    		return;
    	}
    	Veiculo veiculo = new Veiculo();
    	veiculo.setIdVeiculo(tf_Placa.getText());
    	veiculo.setModelo(tf_Modelo.getText());
    	veiculo.setCombustivel(comboBox_Combustivel.getSelectedItem().toString());
    	veiculo.setCor(tf_Cor.getText());
    	veiculo.setStatus("Disponivel");
    	veiculo.setPreco(veiculoController.getValorPreco(tf_Preco.getText()));
    	veiculo.setKilometragem(veiculoController.getValorKilometragem(tf_km.getText()));
    	veiculo.setIdFuncionario(LoginFuncionario.getIdFuncionario());
    	veiculoController.addVeiculo(veiculo);
    	JOptionPane.showMessageDialog(null, "Veiculo cadastrado!");
    }
    
    private void bt_cancelarActionPerformed(java.awt.event.ActionEvent evt) {
        this.dispose();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CadastrarVeiculo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CadastrarVeiculo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CadastrarVeiculo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CadastrarVeiculo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CadastrarVeiculo().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton bt_cadastrar;
    private JButton bt_cancelar;
    @SuppressWarnings("rawtypes")
	private JComboBox comboBox_Combustivel;
    private JLabel label_Preco;
    private JLabel label_Combustivel;
    private JLabel label_km;
    private JLabel label_logo1;
    private JLabel label_logo2;
    private JLabel label_Placa;
    private JLabel label_Cor;
    private JLabel label_Modelo;
    private JPanel panel_label;
    private JPanel panel_principal;
    private JPanel panel_text;
    private JPanel panel_top;
    private JTextField tf_Preco;
    private JTextField tf_km;
    private JFormattedTextField tf_Placa;
    private JTextField tf_Cor;
    private JTextField tf_Modelo;
    private VeiculoController veiculoController;
}
