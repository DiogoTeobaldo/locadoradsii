package views;

import java.awt.event.ActionEvent;
import java.text.ParseException;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.text.MaskFormatter;

import controller.FuncionarioController;
import entity.Funcionario;
import entity.LoginFuncionario;

import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

@SuppressWarnings("serial")
public class CadastrarFuncionario extends JFrame {

	public CadastrarFuncionario() throws ParseException {
		initComponents();
	}

	private void initComponents() throws ParseException {
		funcionarioController = new FuncionarioController();
		panel_top = new JPanel();
		label_logo2 = new JLabel();
		setAutoRequestFocus(false);
		setBackground(new java.awt.Color(204, 204, 204));
		setPreferredSize(new java.awt.Dimension(600, 600));
		setResizable(false);

		panel_top.setForeground(new java.awt.Color(204, 204, 204));

		label_logo2.setFont(new java.awt.Font("Lucida Grande", 0, 18));
		label_logo2.setText("Cadastrar Funcionario");

		GroupLayout panel_topLayout = new GroupLayout(panel_top);
		panel_topLayout.setHorizontalGroup(
			panel_topLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, panel_topLayout.createSequentialGroup()
					.addGap(224)
					.addComponent(label_logo2)
					.addContainerGap(233, Short.MAX_VALUE))
		);
		panel_topLayout.setVerticalGroup(
			panel_topLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, panel_topLayout.createSequentialGroup()
					.addContainerGap(32, Short.MAX_VALUE)
					.addComponent(label_logo2))
		);
		panel_top.setLayout(panel_topLayout);
		label_logo1 = new JLabel();
		
				label_logo1.setFont(new java.awt.Font("Lucida Grande", 1, 36));
				label_logo1.setText("Locadora de Veiculos");
		panel_tf = new JPanel();
		tf_nome = new JTextField();
		tf_cargo = new JTextField();
		tf_turno = new JTextField();
		tf_email = new JTextField();
		tf_telefone = new JFormattedTextField(new MaskFormatter("(##)####-####"));
		tf_cpf = new JFormattedTextField(new MaskFormatter("###.###.###-##"));
		tf_nascimento = new JFormattedTextField(new MaskFormatter("##-##-####"));
		tf_contratacao = new JFormattedTextField(new MaskFormatter("##-##-####"));
		button_cancelar = new JButton();
		button_cadastrar = new JButton();
		tf_salario = new JTextField();
		
				panel_tf.setForeground(new java.awt.Color(204, 204, 204));
				
						button_cancelar.setText("Cancelar");
						button_cancelar.addActionListener(new java.awt.event.ActionListener() {
							public void actionPerformed(java.awt.event.ActionEvent evt) {
								button_cancelarActionPerformed(evt);
							}
						});
						
								button_cadastrar.setText("Cadastrar");
								button_cadastrar.addActionListener(new java.awt.event.ActionListener(){
									public void actionPerformed(java.awt.event.ActionEvent evt) {
										button_cadastrarActionPerformed(evt);
									}
								});
										
										passwordField = new JPasswordField();
										passwordField.setColumns(10);
								
										GroupLayout panel_tfLayout = new GroupLayout(panel_tf);
										panel_tfLayout.setHorizontalGroup(
											panel_tfLayout.createParallelGroup(Alignment.LEADING)
												.addGroup(panel_tfLayout.createSequentialGroup()
													.addGroup(panel_tfLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(tf_cpf, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
														.addComponent(tf_cargo, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)
														.addComponent(tf_turno, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)
														.addComponent(tf_telefone, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
														.addComponent(tf_email, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)
														.addComponent(tf_salario, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
														.addComponent(tf_contratacao, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
														.addComponent(tf_nascimento, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
														.addGroup(panel_tfLayout.createSequentialGroup()
															.addComponent(passwordField)
															.addGap(100)
															.addComponent(button_cadastrar)
															.addPreferredGap(ComponentPlacement.RELATED)
															.addComponent(button_cancelar, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
															.addGap(34))
														.addComponent(tf_nome, 443, 443, 443))
													.addContainerGap())
										);
										panel_tfLayout.setVerticalGroup(
											panel_tfLayout.createParallelGroup(Alignment.LEADING)
												.addGroup(panel_tfLayout.createSequentialGroup()
													.addGap(18)
													.addComponent(tf_nome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED)
													.addComponent(tf_cargo, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED)
													.addComponent(tf_turno, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED)
													.addComponent(tf_email, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED)
													.addComponent(tf_salario, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED)
													.addComponent(tf_cpf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED)
													.addComponent(tf_telefone, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
													.addGap(5)
													.addComponent(tf_nascimento, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
													.addGap(7)
													.addComponent(tf_contratacao, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED)
													.addGroup(panel_tfLayout.createParallelGroup(Alignment.BASELINE)
														.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
														.addComponent(button_cadastrar)
														.addComponent(button_cancelar)))
										);
										panel_tf.setLayout(panel_tfLayout);
		panel_label = new JPanel();
		label_contratacao = new JLabel();
		label_nascimento = new JLabel();
		label_email = new JLabel();
		label_telefone = new JLabel();
		label_cargo = new JLabel();
		label_turno = new JLabel();
		labe_cpf = new JLabel();
		label_nome = new JLabel();
		label_salario = new JLabel();
		
				label_contratacao.setText("Data de Contratacao:");
				
						label_nascimento.setText("Nascimento:");
						
								label_email.setText("Email:");
								
										label_telefone.setText("Telefone:");
										
												label_cargo.setText("Cargo:");
												
														label_turno.setText("Turno:");
														
																labe_cpf.setText("CPF:");
																
																		label_nome.setText("Nome:");
																		
																				label_salario.setText("Salario:");
																						
																						JLabel lblSenha = new JLabel("Senha:");
																				
																						GroupLayout panel_labelLayout = new GroupLayout(panel_label);
																						panel_labelLayout.setHorizontalGroup(
																							panel_labelLayout.createParallelGroup(Alignment.LEADING)
																								.addGroup(panel_labelLayout.createSequentialGroup()
																									.addContainerGap()
																									.addGroup(panel_labelLayout.createParallelGroup(Alignment.LEADING)
																										.addComponent(label_contratacao)
																										.addComponent(label_email)
																										.addComponent(label_telefone)
																										.addComponent(label_cargo)
																										.addComponent(label_turno)
																										.addComponent(label_nascimento)
																										.addComponent(labe_cpf, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
																										.addComponent(label_nome)
																										.addComponent(label_salario)
																										.addComponent(lblSenha))
																									.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
																						);
																						panel_labelLayout.setVerticalGroup(
																							panel_labelLayout.createParallelGroup(Alignment.TRAILING)
																								.addGroup(panel_labelLayout.createSequentialGroup()
																									.addGap(24)
																									.addComponent(label_nome)
																									.addGap(18)
																									.addComponent(label_cargo)
																									.addGap(18)
																									.addComponent(label_turno)
																									.addGap(18)
																									.addComponent(label_email)
																									.addGap(18)
																									.addComponent(label_salario)
																									.addGap(18)
																									.addComponent(labe_cpf)
																									.addGap(18)
																									.addComponent(label_telefone)
																									.addGap(18)
																									.addComponent(label_nascimento)
																									.addGap(18)
																									.addComponent(label_contratacao)
																									.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																									.addComponent(lblSenha))
																						);
																						panel_label.setLayout(panel_labelLayout);

		GroupLayout layout = new GroupLayout(getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(panel_label, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_tf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
				.addGroup(layout.createSequentialGroup()
					.addComponent(panel_top, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(layout.createSequentialGroup()
					.addGap(120)
					.addComponent(label_logo1)
					.addContainerGap(111, Short.MAX_VALUE))
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(label_logo1, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_top, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(layout.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_label, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(panel_tf, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE))
					.addGap(358))
		);
		getContentPane().setLayout(layout);

		pack();
	}

	private void button_cadastrarActionPerformed(ActionEvent evt) {
		if(!funcionarioController.validaCpf(tf_cpf.getText())) {
			JOptionPane.showMessageDialog(null, "CPF invalido! Use somente 11 numeros");
			return;
		}
		if(!funcionarioController.validaNome(tf_nome.getText())) {
			JOptionPane.showMessageDialog(null, "Nome invalido! Use somente letras");
			return;
		}
		if(!funcionarioController.validaCargo(tf_cargo.getText())) {
			JOptionPane.showMessageDialog(null, "Cargo invalido! Use somente letras");
			return;
		}
		if(!funcionarioController.validaTurno(tf_turno.getText())) {
			JOptionPane.showMessageDialog(null, "Turno invalido! Use somente letras");
			return;
		}
		if(!funcionarioController.validaTelefone(tf_telefone.getText())) {
			JOptionPane.showMessageDialog(null, "Telefone invalido! Somente 10 numeros");
			return;
		}
		if(!funcionarioController.validaEmail(tf_email.getText())) {
			JOptionPane.showMessageDialog(null, "Email invalido! Use um email valido");
			return;
		}
		if(!funcionarioController.validaDataNasc(tf_nascimento.getText())) {
			JOptionPane.showMessageDialog(null, "Data de Nascimento invalida! Use somente numeros, dia menor que 32 e mes menor que 13");
			return;
		}
		if(!funcionarioController.validaDataCont(tf_contratacao.getText())) {
			JOptionPane.showMessageDialog(null, "Data de Contratacao invalida! Use somente numeros, dia menor que 32 e mes menor que 13");
			return;
		}
		if(!funcionarioController.validaSalario(tf_salario.getText())) {
			JOptionPane.showMessageDialog(null, "Salario invalido! Use somente numeros maiores que zero");
			return;
		}
		Funcionario funcionario = new Funcionario();
		funcionario.setCargo(tf_cargo.getText());
		funcionario.setTurno(tf_turno.getText());
		funcionario.setSalarioStr(tf_salario.getText());
		funcionario.setData_cont(tf_contratacao.getText());
		funcionario.setNome(tf_nome.getText());
		funcionario.setCpf(tf_cpf.getText().replace("-","").replace(".",""));
		funcionario.setTelefone(tf_telefone.getText().replace("(","").replace(")","").replace("-",""));
		funcionario.setEmail(tf_email.getText());
		funcionario.setData(tf_nascimento.getText());
		funcionario.setSenha(new String(passwordField.getPassword()));
		funcionario.setIdFuncionario(LoginFuncionario.getIdFuncionario());
		funcionarioController.addFuncionario(funcionario);    		
		JOptionPane.showMessageDialog(null, "Funcionario Cadastrado!");
	}

	private void button_cancelarActionPerformed(java.awt.event.ActionEvent evt) {
		this.dispose();
	}

	public static void main(String args[]) {
		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(CadastrarFuncionario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(CadastrarFuncionario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(CadastrarFuncionario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(CadastrarFuncionario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}

		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new CadastrarFuncionario().setVisible(true);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		});
	}

	private JButton button_cadastrar;
	private JButton button_cancelar;
	private JLabel label_salario;
	private JLabel label_cargo;
	private JLabel label_turno;
	private JLabel label_nome;
	private JLabel labe_cpf;
	private JLabel label_email;
	private JLabel label_logo1;
	private JLabel label_logo2;
	private JLabel label_nascimento;
	private JLabel label_telefone;
	private JLabel label_contratacao;
	private JPanel panel_label;
	private JPanel panel_tf;
	private JPanel panel_top;
	private JTextField tf_salario;
	private JTextField tf_cargo;
	private JTextField tf_turno;
	private JFormattedTextField tf_contratacao;
	private JFormattedTextField tf_cpf;
	private JTextField tf_email;
	private JFormattedTextField tf_nascimento;
	private JTextField tf_nome;
	private JFormattedTextField tf_telefone;
	private FuncionarioController funcionarioController;
	private JPasswordField passwordField;
}
