package views;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.text.MaskFormatter;

import controller.FuncionarioController;
import entity.Funcionario;
import entity.LoginFuncionario;

@SuppressWarnings("serial")
public class AtualizarFuncionario extends JFrame {

    public AtualizarFuncionario() {
        initComponents();
    }
    
    private void initComponents() {
    	funcionarioController = new FuncionarioController();
        panel_top = new JPanel();
        label_logo1 = new JLabel();
        label_logo2 = new JLabel();
        panel_principal = new JPanel();
        panel_tf = new JPanel();
        tf_nome = new JTextField();
        tf_cargo = new JTextField();
        tf_turno = new JTextField();
        tf_email = new JTextField();
        try {
        	tf_telefone = new JFormattedTextField(new MaskFormatter("(##)####-####"));
            tf_cpf = new JFormattedTextField(new MaskFormatter("###.###.###-##"));
			tf_nascimento = new JFormattedTextField(new MaskFormatter("##-##-####"));
			tf_contratacao = new JFormattedTextField(new MaskFormatter("##-##-####"));
        } catch (ParseException e1) {
			e1.printStackTrace();
		}
        button_pesquisar = new JButton();
        tf_salario = new JTextField();
        panel_label = new JPanel();
        labe_cpf = new JLabel();
        label_nome = new JLabel();
        label_cargo = new JLabel();
        label_turno = new JLabel();
        label_email = new JLabel();
        label_nascimento = new JLabel();
        label_contratacao = new JLabel();
        label_telefone = new JLabel();
        label_salario = new JLabel();
        button_atualizar = new JButton();
        button_cancelar = new JButton();

        setPreferredSize(new java.awt.Dimension(600, 700));
        setResizable(false);

        panel_top.setForeground(new java.awt.Color(204, 204, 204));

        label_logo1.setFont(new java.awt.Font("Lucida Grande", 1, 36)); // NOI18N
        label_logo1.setText("Locadora de Veiculos");

        label_logo2.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        label_logo2.setText("Atualizar Funcionario");

        GroupLayout panel_topLayout = new GroupLayout(panel_top);
        panel_top.setLayout(panel_topLayout);
        panel_topLayout.setHorizontalGroup(
            panel_topLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_topLayout.createSequentialGroup()
                .addGroup(panel_topLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(panel_topLayout.createSequentialGroup()
                        .addGap(117, 117, 117)
                        .addComponent(label_logo1))
                    .addGroup(panel_topLayout.createSequentialGroup()
                        .addGap(232, 232, 232)
                        .addComponent(label_logo2)))
                .addContainerGap(175, Short.MAX_VALUE))
        );
        panel_topLayout.setVerticalGroup(
            panel_topLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_topLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(label_logo1)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                .addComponent(label_logo2))
        );

        panel_principal.setForeground(new java.awt.Color(204, 204, 204));

        panel_tf.setForeground(new java.awt.Color(204, 204, 204));

        button_pesquisar.setText("Pesquisar");

        GroupLayout panel_tfLayout = new GroupLayout(panel_tf);
        panel_tf.setLayout(panel_tfLayout);
        panel_tfLayout.setHorizontalGroup(
            panel_tfLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_tfLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_tfLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(panel_tfLayout.createSequentialGroup()
                        .addComponent(tf_salario,GroupLayout.PREFERRED_SIZE,200,GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(tf_cpf, GroupLayout.PREFERRED_SIZE, 100,GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_email,GroupLayout.PREFERRED_SIZE,300,GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_cargo,GroupLayout.PREFERRED_SIZE,300,GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_turno,GroupLayout.PREFERRED_SIZE,300,GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_nome)
                    .addGroup(panel_tfLayout.createSequentialGroup()
                        .addGroup(panel_tfLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        	.addComponent(tf_nascimento,GroupLayout.PREFERRED_SIZE,100,GroupLayout.PREFERRED_SIZE)
                            .addComponent(tf_telefone,GroupLayout.PREFERRED_SIZE,100,GroupLayout.PREFERRED_SIZE)
                            .addComponent(button_pesquisar)
                            .addComponent(tf_contratacao,GroupLayout.PREFERRED_SIZE,100,GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 227, Short.MAX_VALUE))))
        );
        panel_tfLayout.setVerticalGroup(
            panel_tfLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_tfLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tf_cpf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(button_pesquisar)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 70, Short.MAX_VALUE)
                .addComponent(tf_nome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tf_cargo, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tf_turno, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tf_email, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tf_salario, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(tf_telefone, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tf_nascimento)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tf_contratacao)
                .addGap(13, 13, 13))
        );

        labe_cpf.setText("CPF:");

        label_nome.setText("Nome:");

        label_cargo.setText("Cargo:");
        
        label_turno.setText("Turno:");

        label_email.setText("Email:");

        label_nascimento.setText("Nascimento:");

        label_contratacao.setText("Data de contratacao:");

        label_telefone.setText("Telefone:");

        label_salario.setText("Salario:");

        GroupLayout panel_labelLayout = new GroupLayout(panel_label);
        panel_label.setLayout(panel_labelLayout);
        panel_labelLayout.setHorizontalGroup(
            panel_labelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_labelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_labelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(labe_cpf, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
                    .addComponent(label_nome)
                    .addComponent(label_cargo)
                    .addComponent(label_turno)
                    .addComponent(label_email)
                    .addComponent(label_telefone)
                    .addComponent(label_nascimento)
                    .addComponent(label_salario))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(GroupLayout.Alignment.TRAILING, panel_labelLayout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(label_contratacao)
                .addContainerGap())
        );
        panel_labelLayout.setVerticalGroup(
            panel_labelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_labelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labe_cpf)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(label_nome)
                .addGap(18, 18, 18)
                .addComponent(label_cargo)
                .addGap(18, 18, 18)
                .addComponent(label_turno)
                .addGap(18, 18, 18)
                .addComponent(label_email)
                .addGap(18, 18, 18)
                .addComponent(label_salario)
                .addGap(18, 18, 18)
                .addComponent(label_telefone)
                .addGap(18, 18, 18)
                .addComponent(label_nascimento)
                .addGap(18, 18, 18)
                .addComponent(label_contratacao)
                .addGap(20, 20, 20))
        );

        GroupLayout panel_principalLayout = new GroupLayout(panel_principal);
        panel_principal.setLayout(panel_principalLayout);
        panel_principalLayout.setHorizontalGroup(
            panel_principalLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_principalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel_label, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(panel_tf, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        panel_principalLayout.setVerticalGroup(
            panel_principalLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_principalLayout.createSequentialGroup()
                .addGroup(panel_principalLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(panel_label, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panel_tf, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        
        button_pesquisar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
					button_pesquisarActionPerformed(evt);
            }
        });
        
        button_atualizar.setText("Atualizar");
        button_atualizar.setEnabled(false);
        button_atualizar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
				button_atualizarActionPerformed(evt);
            }
        });
        
        button_cancelar.setText("Cancelar");
        button_cancelar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                button_cancelarActionPerformed(evt);
            }
        });

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(panel_principal, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panel_top, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(button_atualizar)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(button_cancelar, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panel_top, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(panel_principal, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                	.addComponent(button_atualizar)
                    .addComponent(button_cancelar))
                .addContainerGap())
        );

        pack();
    }
    
    private void button_pesquisarActionPerformed(ActionEvent evt) {
    	if(funcionarioController.validaCpf(tf_cpf.getText())) {
    		Funcionario funcionario = funcionarioController.getFuncionario(tf_cpf.getText());
    		if(funcionario==null) {
    			JOptionPane.showMessageDialog(null, "Nenhum Funcionario encontrado com esse Cpf!");
        	}
        	else {
        		tf_cargo.setText(funcionario.getCargo());
        		tf_turno.setText(funcionario.getTurno());
        		tf_salario.setText(funcionario.getSalarioStr());
        		tf_contratacao.setText(funcionario.getData_contNormal());
        		tf_nome.setText(funcionario.getNome());
        		tf_telefone.setText(funcionario.getTelefone());
        		tf_email.setText(funcionario.getEmail());
        		tf_nascimento.setText(funcionario.getDataNormal());
        		button_atualizar.setEnabled(true);
        	}
    	}
    	else
    		JOptionPane.showMessageDialog(null, "Somente 11 numeros para CPF!");
    }
    
    private void button_atualizarActionPerformed(ActionEvent evt) {
    	if(!funcionarioController.validaCpf(tf_cpf.getText())) {
    		JOptionPane.showMessageDialog(null, "CPF invalido! Use somente 11 numeros");
    		return;
    	}
    	if(!funcionarioController.validaNome(tf_nome.getText())) {
    		JOptionPane.showMessageDialog(null, "Nome invalido! Use somente letras");
    		return;
    	}
    	if(!funcionarioController.validaCargo(tf_cargo.getText())) {
    		JOptionPane.showMessageDialog(null, "Cargo invalido! Use somente letras");
    		return;
    	}
    	if(!funcionarioController.validaTurno(tf_turno.getText())) {
    		JOptionPane.showMessageDialog(null, "Turno invalido! Use somente letras");
    		return;
    	}
    	if(!funcionarioController.validaTelefone(tf_telefone.getText())) {
    		JOptionPane.showMessageDialog(null, "Telefone invalido! Use somente 10 numeros");
    		return;
    	}
    	if(!funcionarioController.validaEmail(tf_email.getText())) {
    		JOptionPane.showMessageDialog(null, "Email invalido! Use um email valido");
    		return;
    	}
    	if(!funcionarioController.validaDataNasc(tf_nascimento.getText())) {
    		JOptionPane.showMessageDialog(null, "Data de Nascimento invalida! Use somente numeros, dia menor que 32 e mes menor que 13");
    		return;
    	}
    	if(!funcionarioController.validaDataCont(tf_contratacao.getText())) {
    		JOptionPane.showMessageDialog(null, "Data de Contratacao invalida! Use somente numeros, dia menor que 32 e mes menor que 13");
    		return;
    	}
    	if(!funcionarioController.validaSalario(tf_salario.getText())) {
    		JOptionPane.showMessageDialog(null, "Salario invalido! Use somente numeros maiores que zero");
    		return;
    	}
    	Funcionario funcionario = new Funcionario();
    	funcionario.setCargo(tf_cargo.getText());
    	funcionario.setTurno(tf_turno.getText());
    	funcionario.setSalarioStr(tf_salario.getText());
    	funcionario.setData_cont(tf_contratacao.getText());
    	funcionario.setNome(tf_nome.getText());
    	funcionario.setCpf(tf_cpf.getText().replace("-","").replace(".",""));
    	funcionario.setTelefone(tf_telefone.getText().replace("(","").replace(")","").replace("-",""));
    	funcionario.setEmail(tf_email.getText());
    	funcionario.setData(tf_nascimento.getText());
    	funcionario.setIdFuncionario(LoginFuncionario.getIdFuncionario());
    	funcionarioController.setFuncionario(funcionario);
    	JOptionPane.showMessageDialog(null, "Funcionario Atualizado!");
    }
    
    private void button_cancelarActionPerformed(ActionEvent evt) {
        this.dispose();
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AtualizarCliente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(AtualizarCliente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(AtualizarCliente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(AtualizarCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AtualizarCliente().setVisible(true);
            }
        });
    }
    
    private JButton button_atualizar;
    private JButton button_cancelar;
    private JButton button_pesquisar;
    private JLabel label_salario;
    private JLabel label_cargo;
    private JLabel label_turno;
    private JLabel label_nome;
    private JLabel labe_cpf;
    private JLabel label_email;
    private JLabel label_logo1;
    private JLabel label_logo2;
    private JLabel label_nascimento;
    private JLabel label_telefone;
    private JLabel label_contratacao;
    private JPanel panel_label;
    private JPanel panel_principal;
    private JPanel panel_tf;
    private JPanel panel_top;
    private JTextField tf_salario;
    private JTextField tf_cargo;
    private JTextField tf_turno;
    private JFormattedTextField tf_contratacao;
    private JFormattedTextField tf_cpf;
    private JTextField tf_email;
    private JFormattedTextField tf_nascimento;
    private JTextField tf_nome;
    private JFormattedTextField tf_telefone;
    private FuncionarioController funcionarioController;
}
