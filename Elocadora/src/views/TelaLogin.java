package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

import controller.FuncionarioController;
import entity.Funcionario;
import entity.LoginFuncionario;

@SuppressWarnings("serial")
public class TelaLogin extends JFrame implements ActionListener {

	private JPanel painelNorte;
	private JPanel painelCentral;
	private JPanel painelCentralNorte;
	private JPanel painelCentralSul;
	private JLabel lblTitulo;
	private JLabel lblSubtitulo;
	private JLabel lblLogin;
	private JLabel lblSenha;
	private JFormattedTextField txtLogin;
	private JPasswordField pswSenha;
	private JButton btnLogin;
	private JButton btnLimpar;
	private JButton btnCancelar;

	public TelaLogin() {
		super("Login do Funcionario");
		setLayout(new BorderLayout(30, 30));

		painelNorte = new JPanel(new GridLayout(2, 1));
		painelCentral = new JPanel(new GridLayout(2, 1));
		painelCentralNorte = new JPanel(new GridLayout(2, 2, 10, 10));
		painelCentralSul = new JPanel(new FlowLayout());

		lblTitulo = new JLabel("Locadora de Veiculos");
		lblTitulo.setFont(new Font("Ubuntu", 0, 30));
		lblTitulo.setHorizontalAlignment((int) CENTER_ALIGNMENT);
		lblSubtitulo = new JLabel("Login do Funcionario");
		lblSubtitulo.setFont(new Font("Ubuntu", 0, 18));
		lblSubtitulo.setHorizontalAlignment((int) CENTER_ALIGNMENT);
		lblLogin = new JLabel("Login: ");
		lblLogin.setFont(new Font("Ubuntu", 0, 16));
		lblSenha = new JLabel("Senha: ");
		lblSenha.setFont(new Font("Ubuntu", 0, 16));
		txtLogin = new JFormattedTextField(30);
		pswSenha = new JPasswordField(30);
		btnLogin = new JButton("Logar");
		btnLimpar = new JButton("Limpar");
		btnCancelar = new JButton("Cancelar");

		try {
			txtLogin = new JFormattedTextField(new MaskFormatter("###.###.###-##"));
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(null, "Digite corretamente o login");
		}

		btnLogin.addActionListener(this);
		btnLimpar.addActionListener(this);
		btnCancelar.addActionListener(this);

		painelNorte.add(lblTitulo);
		painelNorte.add(lblSubtitulo);

		painelNorte.setBorder(new EmptyBorder(10, 0, 0, 0));
		painelCentralNorte.setBorder(new EmptyBorder(20, 150, 30, 130));

		painelCentralNorte.add(lblLogin);
		painelCentralNorte.add(txtLogin);
		painelCentralNorte.add(lblSenha);
		painelCentralNorte.add(pswSenha);

		painelCentralSul.add(btnLogin);
		painelCentralSul.add(btnLimpar);
		painelCentralSul.add(btnCancelar);

		painelCentral.add(painelCentralNorte);
		painelCentral.add(painelCentralSul);

		add(BorderLayout.NORTH, painelNorte);
		add(BorderLayout.CENTER, painelCentral);

		setSize(550, 350);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btnLogin) {
			if(pswSenha.getPassword().length == 0) {
				JOptionPane.showMessageDialog(null, "Digite o Login e a Senha!");
			}
			else {
				FuncionarioController funcionarioCtrl = new FuncionarioController();
				Funcionario funcionario = funcionarioCtrl.getFuncionario(txtLogin.getText());
				if(funcionario != null) {
					if(funcionario.getSenha().equals(new String(pswSenha.getPassword()))) {
						LoginFuncionario.setIdFuncionario(txtLogin.getText().replace(".", "").replace("-", ""));
						LoginFuncionario.setSenha(new String(pswSenha.getPassword()));
						dispose();
						new TelaPrincipal().setVisible(true);
					}
					else
						JOptionPane.showMessageDialog(null, "Login e Senha não correspondem!");
				}
				else
					JOptionPane.showMessageDialog(null, "Funcionario não cadastrado!");
			}
		}
		if(e.getSource() == btnLimpar) {
			txtLogin.setValue("");
			pswSenha.setText("");
		}
		if(e.getSource() == btnCancelar) {
			dispose();
		}
	}

	public static void main(String[] args) {
		new TelaLogin();
	}
}
