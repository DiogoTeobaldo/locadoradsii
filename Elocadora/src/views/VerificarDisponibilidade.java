package views;

/**
 * @author ItaloMota
 */

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import controller.VeiculoController;
import entity.Modelo_Veiculo;
import entity.Veiculo;

@SuppressWarnings("serial")
public class VerificarDisponibilidade extends JFrame implements ActionListener {

	private JPanel painel_norte;
	private JPanel painel_centro;
	private JPanel painel_oeste;
	private JPanel painel_sul;
	private JLabel lbl_titulo;
	private JLabel lbl_subtitulo;
	private JLabel lbl_selecao;
	private JRadioButton radio_nome;
	private JRadioButton radio_marca;
	private JRadioButton radio_placa;
	private JTextField txt_nome;
	private JTextField txt_marca;
	private JFormattedTextField txt_placa;
	private JButton btn_verificar;
	private JButton btn_cancelar;
	private ButtonGroup radio_botoes;
	private JTable tabela;
	private JScrollPane scroll;
	private String[] colunas = {"Id", "Nome", "Marca", "Status", "Preco", "Cor"};
	
	public VerificarDisponibilidade() {
		super("Verificar Disponibilidade");
		
		setLayout(new BorderLayout(20, 20));

		painel_norte = new JPanel(new GridLayout(2, 0));
		painel_centro = new JPanel(new BorderLayout());
		painel_oeste = new JPanel(new GridLayout(7, 0, 10, 10));
		painel_sul = new JPanel(new GridLayout(1, 2, 35, 35));

		lbl_titulo = new JLabel("Locadora de Veiculos");
		lbl_subtitulo = new JLabel("Verificar Disponibilidade do Veiculo");
		lbl_titulo.setFont(new Font("Ubuntu", 0, 30));
		lbl_titulo.setHorizontalAlignment((int) CENTER_ALIGNMENT);
		lbl_subtitulo.setFont(new Font("Ubuntu", 0, 18));
		lbl_subtitulo.setHorizontalAlignment((int) CENTER_ALIGNMENT);
		lbl_selecao = new JLabel("Selecione o criterio de pesquisa:");
		lbl_selecao.setFont(new Font("Ubuntu", 0, 18));

		radio_botoes = new ButtonGroup();
		radio_nome = new JRadioButton("Nome");
		radio_marca = new JRadioButton("Marca");
		radio_placa = new JRadioButton("Placa");
		radio_botoes.add(radio_nome);
		radio_botoes.add(radio_marca);
		radio_botoes.add(radio_placa);
		txt_nome = new JTextField(10);
		txt_marca = new JTextField(10);
		txt_placa = new JFormattedTextField(10);
		btn_verificar = new JButton("Verificar");
		btn_cancelar = new JButton("Cancelar");
		
		try {
			txt_placa = new JFormattedTextField(new MaskFormatter("UUU-####"));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		txt_nome.setEditable(false);
		txt_marca.setEditable(false);
		txt_placa.setEditable(false);		

		radio_nome.setFont(new Font("Ubuntu", 0, 14));
		radio_marca.setFont(new Font("Ubuntu", 0, 14));
		radio_placa.setFont(new Font("Ubuntu", 0, 14));
		btn_verificar.setFont(new Font("Ubuntu", 0, 14));
		btn_cancelar.setFont(new Font("Ubuntu", 0, 14));

		radio_nome.addActionListener(this);
		radio_marca.addActionListener(this);
		radio_placa.addActionListener(this);
		btn_verificar.addActionListener(this);
		btn_cancelar.addActionListener(this);

		painel_norte.add(lbl_titulo);
		painel_norte.add(lbl_subtitulo);
		
		painel_centro.setVisible(false);

		painel_oeste.add(lbl_selecao);
		painel_oeste.add(radio_nome);
		painel_oeste.add(txt_nome);
		painel_oeste.add(radio_marca);
		painel_oeste.add(txt_marca);
		painel_oeste.add(radio_placa);
		painel_oeste.add(txt_placa);
		painel_oeste.add(btn_verificar);
		painel_oeste.add(btn_cancelar);

		painel_sul.add(btn_verificar);
		painel_sul.add(btn_cancelar);
		
		add(BorderLayout.NORTH, painel_norte);
		add(BorderLayout.WEST, painel_oeste);
		add(BorderLayout.CENTER, painel_centro);
		add(BorderLayout.SOUTH, painel_sul);

		setSize(830, 400);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == radio_nome) {
			txt_nome.setEditable(true);
			txt_marca.setEditable(false);
			txt_marca.setText("");
			txt_placa.setEditable(false);
			txt_placa.setText("");
		}
		if(e.getSource() == radio_marca) {
			txt_marca.setEditable(true);
			txt_nome.setEditable(false);
			txt_nome.setText("");
			txt_placa.setEditable(false);
			txt_placa.setText("");
		}
		if(e.getSource() == radio_placa) {
			txt_placa.setEditable(true);
			txt_nome.setEditable(false);
			txt_nome.setText("");
			txt_marca.setEditable(false);
			txt_marca.setText("");
		}
		if(e.getSource() == btn_verificar) {
			VeiculoController veiculoController = new VeiculoController();
			Modelo_Veiculo modeloVeiculo = new Modelo_Veiculo();
			if(radio_nome.isSelected()) {
				painel_centro.setVisible(false);
				painel_centro.removeAll();
				ArrayList<Veiculo> veiculos = veiculoController.findVeiculoNome(txt_nome.getText());
				if(veiculos != null) {
					modeloVeiculo = veiculoController.findModelo(veiculos.get(0).getModelo());
					String[][] dados = new String[veiculos.size()][6];
					for(int i = 0; i < veiculos.size(); i++) {
						dados[i][0] = veiculos.get(i).getIdVeiculo();
						dados[i][1] = modeloVeiculo.getNome();
						dados[i][2] = modeloVeiculo.getMarca();
						dados[i][3] = veiculos.get(i).getStatus(); 
						dados[i][4] = String.valueOf(veiculos.get(i).getPreco()); 
						dados[i][5] = veiculos.get(i).getCor(); 
					}
					tabela = new JTable(dados, colunas);
					tabela.setEnabled(false);
					scroll = new JScrollPane(tabela);
					painel_centro.add(scroll);
					painel_centro.setVisible(true);
				}
				else {
					JOptionPane.showMessageDialog(null, "Veiculo não encontrado!");
					txt_nome.setText("");
				}
			}
			if(radio_marca.isSelected()) {
				painel_centro.setVisible(false);
				painel_centro.removeAll();
				ArrayList<Veiculo> veiculos = veiculoController.findVeiculoMarca(txt_marca.getText());
				if(veiculos != null) {
					modeloVeiculo = veiculoController.findModelo(veiculos.get(0).getModelo());
					String[][] dados = new String[veiculos.size()][6];
					for(int i = 0; i < veiculos.size(); i++) {
						dados[i][0] = veiculos.get(i).getIdVeiculo();
						dados[i][1] = modeloVeiculo.getNome();
						dados[i][2] = modeloVeiculo.getMarca();
						dados[i][3] = veiculos.get(i).getStatus(); 
						dados[i][4] = String.valueOf(veiculos.get(i).getPreco()); 
						dados[i][5] = veiculos.get(i).getCor();
					}
					tabela = new JTable(dados, colunas);
					tabela.setEnabled(false);
					scroll = new JScrollPane(tabela);
					painel_centro.add(scroll);
					painel_centro.setVisible(true);
				}
				else {
					JOptionPane.showMessageDialog(null, "Marca não encontrada!");
					txt_marca.setText("");
				}
			}
			if(radio_placa.isSelected()) {
				painel_centro.setVisible(false);
				painel_centro.removeAll();
				Veiculo veiculo = veiculoController.findVeiculoPlaca(txt_placa.getText());
				if(veiculo != null) {
					modeloVeiculo = veiculoController.findModelo(veiculo.getModelo());
					String[][] dados = {
							{veiculo.getIdVeiculo(), modeloVeiculo.getNome(), modeloVeiculo.getMarca(), veiculo.getStatus(), String.valueOf(veiculo.getPreco()), veiculo.getCor()},
					};
					tabela = new JTable(dados, colunas);
					tabela.setEnabled(false);
					scroll = new JScrollPane(tabela);
					painel_centro.add(scroll);
					painel_centro.setVisible(true);
				}
				else {
					JOptionPane.showMessageDialog(null, "Placa não encontrada");
					txt_placa.setValue("");
				}
			}
		}
		if(e.getSource() == btn_cancelar) {
			dispose();
		}
	}

	public static void main(String[] args) {
		new VerificarDisponibilidade();
	}
}