package views;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

/**
 * @author glauber
 */
@SuppressWarnings("serial")
public class TelaPrincipal extends JFrame {

    public TelaPrincipal() {
        initComponents();
    }

    private void initComponents() {

        panel_funcionario = new JPanel();
        label_funcionario = new JLabel();
        btn_cadastrarFuncionario = new JButton();
        btn_atualizarFuncionario = new JButton();
        panel_topo = new JPanel();
        label_logo1 = new JLabel();
        panel_veiculo = new JPanel();
        labem_veiculo = new JLabel();
        btn_cadastrarVeiculo = new JButton();
        btn_verfDispVeiculo = new JButton();
        btn_alugarVeiculo = new JButton();
        btn_reservarVeiculo = new JButton();
        btn_devolverVeiculo = new JButton();
        btn_atualizarVeiculo = new JButton();
        panel_cliente = new JPanel();
        label_cliente = new JLabel();
        btn_cadastrarCliente = new JButton();
        btn_atualizarCliente = new JButton();
        panel_sistema = new JPanel();
        jLabel1 = new JLabel();
        btn_emitirRelatorio = new JButton();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        
        label_funcionario.setFont(new java.awt.Font("Lucida Grande", 0, 15)); // NOI18N
        label_funcionario.setText("Funcionario");

        btn_cadastrarFuncionario.setText("Cadastrar");
        btn_cadastrarFuncionario.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                try {
					btn_cadastrarFuncionarioActionPerformed(evt);
				} catch (ParseException e) {
					e.printStackTrace();
				}
            }
        });

        btn_atualizarFuncionario.setText("Atualizar");
        btn_atualizarFuncionario.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btn_atualizarFuncionarioActionPerformed(evt);
            }
        });

        GroupLayout panel_funcionarioLayout = new GroupLayout(panel_funcionario);
        panel_funcionario.setLayout(panel_funcionarioLayout);
        panel_funcionarioLayout.setHorizontalGroup(
            panel_funcionarioLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_funcionarioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_funcionarioLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(btn_cadastrarFuncionario, GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                    .addComponent(btn_atualizarFuncionario, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGroup(GroupLayout.Alignment.TRAILING, panel_funcionarioLayout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(label_funcionario, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        panel_funcionarioLayout.setVerticalGroup(
            panel_funcionarioLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_funcionarioLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(label_funcionario)
                .addGap(18, 18, 18)
                .addComponent(btn_cadastrarFuncionario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_atualizarFuncionario)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        label_logo1.setFont(new java.awt.Font("Ubuntu", 0, 30)); // NOI18N
        label_logo1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        label_logo1.setText("Locadora de Veiculos");

        GroupLayout panel_topoLayout = new GroupLayout(panel_topo);
        panel_topo.setLayout(panel_topoLayout);
        panel_topoLayout.setHorizontalGroup(
            panel_topoLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, panel_topoLayout.createSequentialGroup()
                .addContainerGap(145, Short.MAX_VALUE)
                .addComponent(label_logo1)
                .addGap(147, 147, 147))
        );
        panel_topoLayout.setVerticalGroup(
            panel_topoLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_topoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(label_logo1)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        labem_veiculo.setFont(new java.awt.Font("Lucida Grande", 0, 15)); // NOI18N
        labem_veiculo.setText("Veiculo");

        btn_cadastrarVeiculo.setText("Cadastrar");
        btn_cadastrarVeiculo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btn_cadastrarVeiculoActionPerformed(evt);
            }
        });

        btn_verfDispVeiculo.setText("Verificar Disponibilidade");
        btn_verfDispVeiculo.setPreferredSize(new java.awt.Dimension(104, 29));
        btn_verfDispVeiculo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btn_verfDispVeiculoActionPerformed(evt);
            }
        });

        btn_alugarVeiculo.setText("Alugar");
        btn_alugarVeiculo.setPreferredSize(new java.awt.Dimension(104, 29));
        btn_alugarVeiculo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btn_alugarVeiculoActionPerformed(evt);
            }
        });

        btn_reservarVeiculo.setText("Reservar");
        btn_reservarVeiculo.setPreferredSize(new java.awt.Dimension(104, 29));
        btn_reservarVeiculo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btn_reservarVeiculoActionPerformed(evt);
            }
        });

        btn_devolverVeiculo.setText("Devolver");
        btn_devolverVeiculo.setPreferredSize(new java.awt.Dimension(104, 29));
        btn_devolverVeiculo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btn_devolverVeiculoActionPerformed(evt);
            }
        });

        btn_atualizarVeiculo.setText("Atualizar");
        btn_atualizarVeiculo.setPreferredSize(new java.awt.Dimension(104, 29));
        btn_atualizarVeiculo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btn_atualizarVeiculoActionPerformed(evt);
            }
        });

        GroupLayout panel_veiculoLayout = new GroupLayout(panel_veiculo);
        panel_veiculo.setLayout(panel_veiculoLayout);
        panel_veiculoLayout.setHorizontalGroup(
            panel_veiculoLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(btn_reservarVeiculo, GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
            .addComponent(btn_cadastrarVeiculo, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btn_verfDispVeiculo, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btn_alugarVeiculo, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btn_devolverVeiculo, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(GroupLayout.Alignment.TRAILING, panel_veiculoLayout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(labem_veiculo, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34))
            .addComponent(btn_atualizarVeiculo, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        panel_veiculoLayout.setVerticalGroup(
            panel_veiculoLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_veiculoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labem_veiculo)
                .addGap(18, 18, 18)
                .addComponent(btn_cadastrarVeiculo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_atualizarVeiculo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(btn_verfDispVeiculo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_alugarVeiculo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_reservarVeiculo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_devolverVeiculo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(40, Short.MAX_VALUE))
        );

        label_cliente.setFont(new java.awt.Font("Lucida Grande", 0, 15)); // NOI18N
        label_cliente.setText("Cliente");

        btn_cadastrarCliente.setText("Cadastrar");
        btn_cadastrarCliente.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btn_cadastrarClienteActionPerformed(evt);
            }
        });

        btn_atualizarCliente.setText("Atualizar");
        btn_atualizarCliente.setPreferredSize(new java.awt.Dimension(104, 29));
        btn_atualizarCliente.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btn_atualizarClienteActionPerformed(evt);
            }
        });

        GroupLayout panel_clienteLayout = new GroupLayout(panel_cliente);
        panel_cliente.setLayout(panel_clienteLayout);
        panel_clienteLayout.setHorizontalGroup(
            panel_clienteLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_clienteLayout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(label_cliente, GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(39, Short.MAX_VALUE))
            .addComponent(btn_atualizarCliente, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btn_cadastrarCliente, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        panel_clienteLayout.setVerticalGroup(
            panel_clienteLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_clienteLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(label_cliente)
                .addGap(18, 18, 18)
                .addComponent(btn_cadastrarCliente)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_atualizarCliente, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 0, 15)); // NOI18N
        jLabel1.setText("Sistema");

        btn_emitirRelatorio.setText("Emitir Relatorio");
        btn_emitirRelatorio.setPreferredSize(new java.awt.Dimension(104, 29));

        GroupLayout panel_sistemaLayout = new GroupLayout(panel_sistema);
        panel_sistema.setLayout(panel_sistemaLayout);
        panel_sistemaLayout.setHorizontalGroup(
            panel_sistemaLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panel_sistemaLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(jLabel1)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panel_sistemaLayout.createSequentialGroup()
                .addComponent(btn_emitirRelatorio, GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        panel_sistemaLayout.setVerticalGroup(
            panel_sistemaLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, panel_sistemaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btn_emitirRelatorio, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panel_topo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panel_funcionario, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(panel_veiculo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(panel_cliente, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panel_sistema, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel_topo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(panel_cliente, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(panel_funcionario, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(panel_veiculo, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(35, 35, 35)))
                        .addContainerGap())
                    .addComponent(panel_sistema, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        pack();
        setLocationRelativeTo(null);
    }

    private void btn_cadastrarClienteActionPerformed(ActionEvent evt) {
        CadastrarCliente cadastroCliente = new CadastrarCliente();
        cadastroCliente.setVisible(true);
    }

    private void btn_cadastrarFuncionarioActionPerformed(ActionEvent evt) throws ParseException {
        CadastrarFuncionario cadastrarFuncionario = new CadastrarFuncionario();
        cadastrarFuncionario.setVisible(true);
    }

    private void btn_atualizarFuncionarioActionPerformed(ActionEvent evt) {
        AtualizarFuncionario atualizarFuncionario = new AtualizarFuncionario();
        atualizarFuncionario.setVisible(true);
    }

    private void btn_verfDispVeiculoActionPerformed(ActionEvent evt) {
        VerificarDisponibilidade verificar = new VerificarDisponibilidade();
        verificar.setVisible(true);
    }

    private void btn_atualizarClienteActionPerformed(ActionEvent evt) {
        AtualizarCliente atualizarC = new AtualizarCliente();
        atualizarC.setVisible(true);
    }

    private void btn_cadastrarVeiculoActionPerformed(ActionEvent evt) {
        CadastrarVeiculo cadastrarV = new CadastrarVeiculo();
        cadastrarV.setVisible(true);
    }

    private void btn_devolverVeiculoActionPerformed(ActionEvent evt) {
        RealizarDevolucao realizarD = new RealizarDevolucao();
        realizarD.setVisible(true);
    }

    private void btn_alugarVeiculoActionPerformed(ActionEvent evt) {
        RealizarLocacao realizarL = new RealizarLocacao();
        realizarL.setVisible(true);
    }

    private void btn_reservarVeiculoActionPerformed(ActionEvent evt) {
        RealizarReserva realizarR = new RealizarReserva();
        realizarR.setVisible(true);
    }

    private void btn_atualizarVeiculoActionPerformed(ActionEvent evt) {
        AtualizarVeiculo atualizarV = new AtualizarVeiculo();
        atualizarV.setVisible(true);
    }

    public static void main(String args[]) {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(TelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(TelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            Logger.getLogger(TelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaPrincipal().setVisible(true);
            }
        });
    }

    private JButton btn_alugarVeiculo;
    private JButton btn_atualizarCliente;
    private JButton btn_atualizarFuncionario;
    private JButton btn_atualizarVeiculo;
    private JButton btn_cadastrarCliente;
    private JButton btn_cadastrarFuncionario;
    private JButton btn_cadastrarVeiculo;
    private JButton btn_devolverVeiculo;
    private JButton btn_emitirRelatorio;
    private JButton btn_reservarVeiculo;
    private JButton btn_verfDispVeiculo;
    private JLabel jLabel1;
    private JLabel label_cliente;
    private JLabel label_funcionario;
    private JLabel label_logo1;
    private JLabel labem_veiculo;
    private JPanel panel_cliente;
    private JPanel panel_funcionario;
    private JPanel panel_sistema;
    private JPanel panel_topo;
    private JPanel panel_veiculo;
}
