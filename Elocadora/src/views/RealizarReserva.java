package views;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.text.MaskFormatter;

import controller.ReservaController;
import entity.Reserva;
/**
 * @author glauber, LeonardoMacedo
 */

@SuppressWarnings("serial")
public class RealizarReserva extends JFrame implements ActionListener{
	private JButton btnCancelar;
	private JButton btnReservar;
	private JLabel lblLocadoraVeiculos;
	private JLabel lblRealizarReserva;
	private JLabel lblCliente;
	private JLabel lblVeiculo;
	private JLabel lblData;
	private JLabel lblHora;
	private JFormattedTextField cpfCliente;
	private JFormattedTextField placa;
	private JFormattedTextField data;
	private JFormattedTextField hora;
	private ReservaController reservaController;

	public RealizarReserva() {
		initComponents();
	}

	private void initComponents() {

		btnCancelar = new JButton();
		btnReservar = new JButton();
		lblLocadoraVeiculos = new JLabel();
		lblRealizarReserva = new JLabel();
		lblCliente = new JLabel();
		lblVeiculo = new JLabel();
		lblData = new JLabel();
		lblHora = new JLabel();
		reservaController = new ReservaController();

		lblLocadoraVeiculos.setFont(new Font("Ubuntu", 0, 30));
		lblLocadoraVeiculos.setText("Locadora de Veiculos");

		lblRealizarReserva.setText("Realizar Reserva");

		lblCliente.setText("Cpf do Cliente: ");

		lblVeiculo.setText("Placa do Veiculo:");

		lblData.setText("Data:");

		lblHora.setText("Hora:");

		btnReservar.addActionListener(this);
		btnCancelar.addActionListener(this);
		
		btnCancelar.setText("Cancelar");
		btnReservar.setText("Reservar");


		try {
			cpfCliente = new JFormattedTextField(new MaskFormatter("###.###.###-##"));
			placa = new JFormattedTextField(new MaskFormatter("UUU-####"));
			data = new JFormattedTextField(new MaskFormatter("## - ## - ####"));
			hora = new JFormattedTextField(new MaskFormatter("## : ##"));
		} catch (ParseException e) {
			e.printStackTrace();
		}


		GroupLayout layout = new GroupLayout(getContentPane());
		layout.setHorizontalGroup(
				layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addGroup(layout.createParallelGroup(Alignment.TRAILING)
								.addGroup(layout.createSequentialGroup()
										.addGroup(layout.createParallelGroup(Alignment.LEADING)
												.addGroup(layout.createSequentialGroup()
														.addGap(63)
														.addComponent(lblLocadoraVeiculos))
														.addGroup(layout.createSequentialGroup()
																.addGap(150)
																.addComponent(lblRealizarReserva))
																.addGroup(layout.createSequentialGroup()
																		.addContainerGap()
																		.addGroup(layout.createParallelGroup(Alignment.LEADING)
																				.addComponent(lblVeiculo)
																				.addComponent(lblCliente)
																				.addComponent(lblData)
																				.addComponent(lblHora))
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addGroup(layout.createParallelGroup(Alignment.LEADING, false)
																						.addComponent(placa, GroupLayout.DEFAULT_SIZE, 111, Short.MAX_VALUE)
																						.addComponent(cpfCliente)
																						.addComponent(data)
																						.addComponent(hora))))
																						.addGap(0, 59, Short.MAX_VALUE))
																						.addGroup(layout.createSequentialGroup()
																								.addGap(0, 270, Short.MAX_VALUE)
																								.addComponent(btnReservar)
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(btnCancelar)))
																								.addContainerGap())
				);
		layout.setVerticalGroup(
				layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(lblLocadoraVeiculos)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(lblRealizarReserva)
						.addGap(18)
						.addGroup(layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblCliente)
								.addComponent(cpfCliente, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(layout.createParallelGroup(Alignment.BASELINE)
										.addComponent(lblVeiculo)
										.addComponent(placa, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(layout.createParallelGroup(Alignment.BASELINE)
												.addComponent(data, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(lblData))
												.addPreferredGap(ComponentPlacement.RELATED)
												.addGroup(layout.createParallelGroup(Alignment.BASELINE)
														.addComponent(lblHora)
														.addComponent(hora, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
														.addPreferredGap(ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
														.addGroup(layout.createParallelGroup(Alignment.BASELINE)
																.addComponent(btnCancelar)
																.addComponent(btnReservar))
																.addGap(26))
				);
		getContentPane().setLayout(layout);

		pack();
	}

	public static void main(String args[]) {
		new RealizarReserva().setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnReservar) {
			if(!reservaController.validaCpf(cpfCliente.getText().toString())) {
				JOptionPane.showMessageDialog(null, "CPF invalido!");
				return;
			}
			if(!reservaController.validaPlaca(placa.getText().toString())) {
				JOptionPane.showMessageDialog(null, "Placa de carro invalida!");
				return;
			}
			if(!reservaController.validaData(data.getText().toString().replaceAll(" ", ""))) {
				JOptionPane.showMessageDialog(null, "Data invalida!");
				return;
			}
			if(!reservaController.validaHora(hora.getText().toString())) {
				JOptionPane.showMessageDialog(null, "Hora invalida!");
				return;
			}
			if(!reservaController.isdisponivelVeiculo(placa.getText().toString())){
				JOptionPane.showMessageDialog(null, "Veiculo indisponivel");
				return;
			}
			if(!reservaController.existClinte(cpfCliente.getText().toString().replaceAll("-", "")
					.replace(".", ""))){
				JOptionPane.showMessageDialog(null, "Não existe nenhum cliente cadastrado" + 
						" com esse cpf!");
				return;
			}
			Reserva reserva = new Reserva();
			reserva.setData(data.getText().toString().replaceAll(" ", ""));
			reserva.setHora(hora.getText().toString().replaceAll(" ", ""));
			reserva.setidCliente(cpfCliente.getText().toString().replace(".", "")
					.replaceAll("-", ""));
			reserva.setidVeiculo(placa.getText().toString());
			
			reservaController.addReserva(reserva);
			JOptionPane.showMessageDialog(null, "Reserva efetuada com sucesso!");
		}
		if (e.getSource() == btnCancelar)
			dispose();
	}
}
