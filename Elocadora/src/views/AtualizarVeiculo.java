package views;

/**
 *
 * @author diogoTeobaldo
 */

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

import controller.VeiculoController;
import entity.LoginFuncionario;
import entity.Veiculo;

public class AtualizarVeiculo extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;
	private JPanel painel;
	private JTextField kilometragem;
	private JTextField preco;
	private JTextField cor;
	private JFormattedTextField placa;
	private JComboBox<String> combustivelCombo;
	private JLabel lblCombustivel;
	private JLabel lbl_preco;
	private JLabel lbl_km;
	private JLabel lbl_cor;
	private JButton btnAtualizar;
	private JButton btnCancelar;
	private JButton btnBuscar;
	private JButton btnRemover;
	private VeiculoController veiculoController; 

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AtualizarVeiculo frame = new AtualizarVeiculo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
        
	public AtualizarVeiculo() {
		setBounds(100, 100, 450, 300);
		painel = new JPanel();
		painel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(painel);
		painel.setLayout(new BorderLayout(40, 40));
		
		JLabel lblDadosDoVeiculo = new JLabel("Atualizar Veiculo");
		lblDadosDoVeiculo.setFont(new Font("Dialog", Font.BOLD, 20));
		lblDadosDoVeiculo.setHorizontalAlignment(SwingConstants.CENTER);
		painel.add(lblDadosDoVeiculo, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		painel.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(7, 2, 10, 10));
		
		JLabel lblNewLabel = new JLabel("Placa: ");
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 16));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblNewLabel);
		
		
		try {
			placa = new JFormattedTextField(new MaskFormatter("UUU-####"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		placa.setToolTipText("Numero da Placa");
		panel.add(placa);
		placa.setColumns(10);
		
		lblCombustivel = new JLabel("Combustivel: ");
		lblCombustivel.setFont(new Font("Dialog", Font.BOLD, 16));
		lblCombustivel.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblCombustivel);
		lblCombustivel.setVisible(false);
		
		String [] combustivel = new String[] {"Alcool", "Gasolina", "GNV"}; 
		combustivelCombo = new JComboBox<String>(combustivel);
		panel.add(combustivelCombo);
		combustivelCombo.setVisible(false);
		
		lbl_km = new JLabel("Kilometragem:");
		lbl_km.setFont(new Font("Dialog", Font.BOLD, 16));
		lbl_km.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lbl_km);
		lbl_km.setVisible(false);
		
		kilometragem = new JTextField();
		kilometragem.setToolTipText("Kilometragem");
		panel.add(kilometragem);
		kilometragem.setColumns(10);
		kilometragem.setVisible(false);
		
		lbl_preco = new JLabel("Preco:");
		lbl_preco.setFont(new Font("Dialog", Font.BOLD, 16));
		lbl_preco.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lbl_preco);
		lbl_preco.setVisible(false);
		
		preco = new JTextField();
		preco.setToolTipText("Preco");
		panel.add(preco);
		preco.setColumns(10);
		preco.setVisible(false);
		
		lbl_cor = new JLabel("Cor:");
		lbl_cor.setFont(new Font("Dialog", Font.BOLD, 16));
		lbl_cor.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lbl_cor);
		lbl_cor.setVisible(false);
		
		cor = new JTextField();
		cor.setToolTipText("Cor do Veiculo");
		panel.add(cor);
		cor.setColumns(10);
		cor.setVisible(false);
		
		JPanel panel_1 = new JPanel();
		painel.add(panel_1, BorderLayout.SOUTH);
		panel_1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		btnAtualizar = new JButton("Atualizar");
		btnAtualizar.addActionListener(this);
		panel_1.add(btnAtualizar);
		btnAtualizar.setVisible(false);
		
		btnRemover = new JButton("Remover");
		btnRemover.addActionListener(this);
		panel_1.add(btnRemover);
		btnRemover.setVisible(false);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(this);
		panel_1.add(btnBuscar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(this);
		panel_1.add(btnCancelar);
		
		veiculoController = new VeiculoController();
		
		setSize(450, 500);
		setResizable(false);
		setLocationRelativeTo(null);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnBuscar){
			if(!veiculoController.validaPlaca(placa.getText().toString())){
				JOptionPane.showMessageDialog(null, "Placa de carro invalida!");
			} else {
				Veiculo veiculo = veiculoController.findVeiculoPlaca(placa.getText().toString()); 
				if (veiculo != null){
					trocarVisibilidade();
					combustivelCombo.setSelectedItem(veiculo.getCombustivel());
					cor.setText(veiculo.getCor());
					preco.setText(String.valueOf(veiculo.getPreco()));
					kilometragem.setText(String.valueOf(veiculo.getKilometragem()));
					placa.setEditable(false);
				} else {
					JOptionPane.showMessageDialog(null, "Nenhum veiculo encontrado com essa placa!");
				}
			}
		}
		if (e.getSource() == btnCancelar){
			dispose();
		}
		if (e.getSource() == btnAtualizar){
			Veiculo veiculo = new Veiculo();
			veiculo.setIdVeiculo(placa.getText().toString());
			veiculo.setCombustivel(combustivelCombo.getSelectedItem().toString());
			veiculo.setCor(cor.getText().toString());
			veiculo.setPreco(veiculoController.getValorPreco(preco.getText().toString()));
			veiculo.setKilometragem(veiculoController.getValorKilometragem(kilometragem.getText().toString()));
			veiculo.setIdFuncionario(LoginFuncionario.getIdFuncionario());
			veiculoController.updateVeiculo(veiculo);
			JOptionPane.showMessageDialog(null, "Veiculo atualizado!");
		}
		if (e.getSource() == btnRemover){
			veiculoController.deleteVeiculo(placa.getText().toString());
			trocarVisibilidade();
			placa.setText(null);
			placa.setEditable(true);
			JOptionPane.showMessageDialog(null, "Veiculo removido!");
		}
	}
	
	private void trocarVisibilidade(){
		combustivelCombo.setVisible(!combustivelCombo.isVisible());
		lblCombustivel.setVisible(!lblCombustivel.isVisible());
		lbl_cor.setVisible(!lbl_cor.isVisible());
		cor.setVisible(!cor.isVisible());
		lbl_preco.setVisible(!lbl_preco.isVisible());
		preco.setVisible(!preco.isVisible());
		lbl_km.setVisible(!lbl_km.isVisible());
		kilometragem.setVisible(!kilometragem.isVisible());
		btnBuscar.setVisible(!btnBuscar.isVisible());
		btnAtualizar.setVisible(!btnAtualizar.isVisible());
		btnRemover.setVisible(!btnRemover.isVisible());
	}
}