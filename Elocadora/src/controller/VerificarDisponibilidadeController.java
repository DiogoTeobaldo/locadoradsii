package controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
* @author ItaloMota
*/

public class VerificarDisponibilidadeController {
	
	public boolean validaPlaca(String placa) {
		placa = placa.replace('-', ' ');
		Pattern pattern = Pattern.compile("[A-Z]{3,3}\\s\\d{4,4}");
		Matcher matcher = pattern.matcher(placa);
		
		return matcher.matches();
	}

}
