package controller;


import DAO.FuncionarioDAO;
import entity.Funcionario;

public class FuncionarioController {
	
	private Funcionario funcionario;
	private FuncionarioDAO funcionarioDAO;
	
	public FuncionarioController() {
		funcionario = new Funcionario();
		funcionarioDAO = new FuncionarioDAO();
	}
	
	public void addFuncionario(Funcionario f) {
		funcionarioDAO.addFuncionario(f);
	}
	
	public Funcionario getFuncionario(String cpf) {
		return funcionarioDAO.getFuncionario(cpf.replace(".","").replace("-",""));
	}
	
	public void setFuncionario(Funcionario f) {
		funcionarioDAO.setFuncionario(f);
	}
	
	public boolean validaNome(String nome) {
		return funcionario.validaTexto(nome);
	}
	
	public boolean validaCargo(String cargo) {
		return funcionario.validaTexto(cargo);
	}
	
	public boolean validaTurno(String turno) {
		return funcionario.validaTexto(turno);
	}
	public boolean validaCpf(String cpf) {
		return funcionario.validaCpf(cpf);
	}
	
	public boolean validaTelefone(String telefone) {
		return funcionario.validaTelefone(telefone);
	}
	
	public boolean validaEmail(String email) {
		return funcionario.validaEmail(email);
	}
	
	public boolean validaDataNasc(String datan) {		
		return funcionario.validaData(datan);
	}
	
	public boolean validaDataCont(String datac) {		
		return funcionario.validaData(datac);
	}
	
	public boolean validaSalario(String salario) {
		return funcionario.validaSalario(salario);
	}
	
}
