package controller;

import java.util.ArrayList;

import DAO.ModeloDAO;
import DAO.VeiculoDAO;
import entity.Modelo_Veiculo;
import entity.Veiculo;

/**
 *
 * @author diogoTeobaldo
 */
public class VeiculoController {
	
	private VeiculoDAO veiculoDAO;
	private ModeloDAO modeloDAO;
	private Veiculo veiculo;
	
	public VeiculoController() {
		veiculoDAO = new VeiculoDAO();
		modeloDAO = new ModeloDAO();
		veiculo = new Veiculo();
	}
	
	public boolean validaPlaca(String placa){
		return veiculo.validaPlaca(placa);
	}
	
	public void addVeiculo(Veiculo veiculo){
		veiculoDAO.addVeiculo(veiculo);
	}
	
	public Modelo_Veiculo findModelo(String idModelo) {
		return modeloDAO.findModelo(idModelo);
	}
	
	public Veiculo findVeiculoPlaca(String idveiculo){
		return veiculoDAO.findVeiculoPlaca(idveiculo);
	}
	
	public ArrayList<Veiculo> findVeiculoNome(String nome) {
		return veiculoDAO.findVeiculoNome(nome);
	}
	
	public ArrayList<Veiculo> findVeiculoMarca(String marca) {
		return veiculoDAO.findVeiculoMarca(marca);
	}
	
	public void updateVeiculo(Veiculo veiculo){
		veiculoDAO.updateVeiculo(veiculo);
	}	
	
	public void deleteVeiculo(String idveiculo){
		veiculoDAO.deleteVeiculo(idveiculo);
	}
	
	public float getValorPreco(String preco){
		return veiculo.getValorPreco(preco);
	}
	
	public float getValorKilometragem(String km){
		return veiculo.getValorKilometragem(km);
	}
}
