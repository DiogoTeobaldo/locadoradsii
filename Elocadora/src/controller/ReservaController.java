package controller;

/**
* @author LeonardoMacedo
*/

import DAO.ClienteDAO;
import DAO.ReservaDAO;
import DAO.VeiculoDAO;
import entity.Reserva;
import entity.Veiculo;

public class ReservaController {
	private Reserva reserva;
	private VeiculoDAO veiculoDAO;
	private ReservaDAO reservaDAO;
	private ClienteDAO clienteDAO;
	
	public ReservaController(){
		reserva = new Reserva();
		veiculoDAO = new VeiculoDAO();
		reservaDAO = new ReservaDAO();
		clienteDAO = new ClienteDAO();
	}
	
	public boolean validaCpf(String idCliente) {
		return reserva.validaIdCliente(idCliente);
	}
	public boolean validaPlaca(String placa) {
		return reserva.validaIdVeiculo(placa);
	}
	public boolean validaData(String data) {
		return reserva.validaData(data);
	}
	public boolean validaHora(String hora) {
		return reserva.validaHora(hora);
	}
	
	public boolean isdisponivelVeiculo(String placa){
		Veiculo veiculo = new Veiculo();
		veiculo = veiculoDAO.findVeiculoPlaca(placa);
		if (veiculo != null)
			return veiculo.getStatus().equals("Disponivel");
		return false;
	}
	
	public boolean existClinte(String cpf){
		return clienteDAO.existCliente(cpf);
	}
	
	public void addReserva(Reserva reserva){
		reservaDAO.addReserva(reserva);
	}
}
