/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

/**
 *
 * @author gilbertoalexsantos
 */
public class Locacao {
    private float preco;
    private Data dataInicial, dataFinal;
    private String idFuncionario;

    public Locacao(float preco, Data dataInicial, Data dataFinal) {
        this.preco = preco;
        this.dataInicial = dataInicial;
        this.dataFinal = dataFinal;
    }

    public float getPreco() {
        return preco;
    }

    public Data getDataInicial() {
        return dataInicial;
    }

    public Data getDataFinal() {
        return dataFinal;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

    public void setDataInicial(Data dataInicial) {
        this.dataInicial = dataInicial;
    }

    public void setDataFinal(Data dataFinal) {
        this.dataFinal = dataFinal;
    }        
    
    public String getIdFuncionario() {
    	return idFuncionario;
    }
    
    public void setIdFuncionario(String idFuncionario) {
    	this.idFuncionario = idFuncionario;
    }
}
