/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

/**
 *
 * @author gilbertoalexsantos
 */
public class Modelo_Veiculo {
    private String nome;
    private String marca;
    private int ano;
    private String idFuncionario;
    
    public Modelo_Veiculo() {}

    public Modelo_Veiculo(String nome, String marca, int ano) {
        this.nome = nome;
        this.marca = marca;
        this.ano = ano;
    }

    public String getNome() {
        return nome;
    }

    public String getMarca() {
        return marca;
    }

    public int getAno() {
        return ano;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }
    
    public String getIdFuncionario() {
    	return idFuncionario;
    }
    
    public void setIdFuncionario(String idFuncionario) {
    	this.idFuncionario = idFuncionario;
    }
    
}
