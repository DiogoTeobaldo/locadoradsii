package entity;

public class LoginFuncionario {
	
	private static String idFuncionario;
	private static String senha;
	
	public static String getIdFuncionario() {
		return idFuncionario;
	}
	public static void setIdFuncionario(String idFuncionario) {
		LoginFuncionario.idFuncionario = idFuncionario;
	}
	public static String getSenha() {
		return senha;
	}
	public static void setSenha(String senha) {
		LoginFuncionario.senha = senha;
	}
}
