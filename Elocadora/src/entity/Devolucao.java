/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

/**
 *
 * @author gilbertoalexsantos
 */
public class Devolucao {
    private Data dataFinalPrevista, dataFinalEfetiva;
    private Hora horaFinalPrevista, horaFinalEfetiva;
    private String idFuncionario;

    public Devolucao(Data dataFinalPrevista, Data dataFinalEfetiva, Hora horaFinalPrevista, Hora horaFinalEfetiva) {
        this.dataFinalPrevista = dataFinalPrevista;
        this.dataFinalEfetiva = dataFinalEfetiva;
        this.horaFinalPrevista = horaFinalPrevista;
        this.horaFinalEfetiva = horaFinalEfetiva;
        idFuncionario = "";
    }

    public Data getDataFinalPrevista() {
        return dataFinalPrevista;
    }

    public Data getDataFinalEfetiva() {
        return dataFinalEfetiva;
    }

    public Hora getHoraFinalPrevista() {
        return horaFinalPrevista;
    }

    public Hora getHoraFinalEfetiva() {
        return horaFinalEfetiva;
    }

    public void setDataFinalPrevista(Data dataFinalPrevista) {
        this.dataFinalPrevista = dataFinalPrevista;
    }

    public void setDataFinalEfetiva(Data dataFinalEfetiva) {
        this.dataFinalEfetiva = dataFinalEfetiva;
    }

    public void setHoraFinalPrevista(Hora horaFinalPrevista) {
        this.horaFinalPrevista = horaFinalPrevista;
    }

    public void setHoraFinalEfetiva(Hora horaFinalEfetiva) {
        this.horaFinalEfetiva = horaFinalEfetiva;
    }
    
    public String getIdFuncionario() {
    	return idFuncionario;
    }
    
    public void setIdFuncionario(String idFuncionario) {
    	this.idFuncionario = idFuncionario;
    }
    
}
