package entity;

/**
 *
 * @author gilbertoalexsantos
 */
public enum Combustivel { 
    GASOLINA, ALCOOL, GNV;
}
