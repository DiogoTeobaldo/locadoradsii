/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

/**
 *
 * @author gilbertoalexsantos
 */
public class Nota {
    private String nome, cpf;
    private float valor;
    private Data data;

    public Nota(String nome, String cpf, float valor, Data data) {
        this.nome = nome;
        this.cpf = cpf;
        this.valor = valor;
        this.data = data;
    }

    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public float getValor() {
        return valor;
    }

    public Data getData() {
        return data;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public void setData(Data data) {
        this.data = data;
    }
    
}
