package entity;

public class Funcionario extends Pessoa {

    private String cargo;
    private String turno; 
    private float salario;
    private String senha;
    private String data_cont;
    private String idFuncionario;
    
	public String getCargo() {
        return cargo;
    }

    public String getTurno() {
        return turno;
    }

    public float getSalario() {
        return salario;
    }
    
    public String getSenha() {
    	return senha;
    }
    
    public String getSalarioStr() {
    	return Float.toString(salario);
    }
    
    public String getData_cont() {    	
        if(data_cont!=null)
        	return data_cont.substring(6, 10)+data_cont.substring(2, 6)+data_cont.substring(0, 2);
        else
        	return null;
    }
    
    public String getData_contNormal() {
    	if(data_cont!=null)
    		return data_cont.substring(8,10)+data_cont.substring(4,8)+data_cont.substring(0,4);
    	else
    		return null;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }
    
    public void setSalario(float salario) {
    	this.salario = salario;
    }
    
    public void setSenha(String senha) {
    	this.senha = senha;
    }
    
    public void setSalarioStr(String salario) {
        this.salario = converterSalario(salario);
    }

    public void setData_cont(String data_cont) {
        this.data_cont = data_cont;
    }
    
    public String getIdFuncionario() {
    	return idFuncionario;
    }
    
    public void setIdFuncionario(String idFuncionario) {
    	this.idFuncionario = idFuncionario;
    }
    
    public boolean validaTexto(String texto) {
		if(!texto.matches("[a-zA-Z ]+"))
			return false;
		return true;
	}
    
    public boolean validaCpf(String cpf) {
    	if(!cpf.replace("-","").replace(".","").matches("[0-9]{11}"))
    		return false;
		return true;			
	}
    
    public boolean validaTelefone(String telefone) {
		if(!telefone.replace("(","").replace(")","").replace("-","").matches("[0-9]{10}"))
    		return false;
		return true;
	}
	
	public boolean validaEmail(String email) {
		if(!email.matches("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}"))
    		return false;
		return true;
	}
	
	public boolean validaData(String data) {
		if (!data.matches("[0-9]{2,2}-[0-9]{2,2}-[0-9]{4,4}"))
			return false;
		else{
			int valor = Integer.parseInt(data.substring(0, 2));
			boolean valido = true;
			if (valor < 0 || valor > 32)
				valido = false;
			valor = Integer.parseInt(data.substring(3, 5));
			if (valor < 0 || valor > 12)
				valido = false;
			return valido;
		}		
	}
	
	public boolean validaSalario(String salario) {
		if(converterSalario(salario)<0)
			return false;
		return true;
	}
	
	private float converterSalario(String salario) {
		try {
			return Float.parseFloat(salario);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return -1;
	}
	
}
