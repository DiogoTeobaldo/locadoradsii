/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

/**
 *
 * @author gilbertoalexsantos
 */
public class Descricao_Veiculo {
    private float kmInicial, kmFinal, preco;
    private EstadoVeiculo estadoVeiculo; 

    public Descricao_Veiculo(float kmInicial, float kmFinal, float preco, EstadoVeiculo estadoVeiculo) {
        this.kmInicial = kmInicial;
        this.kmFinal = kmFinal;
        this.preco = preco;
        this.estadoVeiculo = estadoVeiculo;
    }

    public float getKmInicial() {
        return kmInicial;
    }

    public float getKmFinal() {
        return kmFinal;
    }

    public float getPreco() {
        return preco;
    }

    public void setKmInicial(float kmInicial) {
        this.kmInicial = kmInicial;
    }

    public void setKmFinal(float kmFinal) {
        this.kmFinal = kmFinal;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

    public void setEstadoVeiculo(EstadoVeiculo estadoVeiculo) {
        this.estadoVeiculo = estadoVeiculo;
    }

    public EstadoVeiculo getEstadoVeiculo() {
        return estadoVeiculo;
    }
    
    
}
