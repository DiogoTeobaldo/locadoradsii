/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

/**
 *
 * @author gilbertoalexsantos
 */
public class Pagamento {
    private boolean forma;
    private String status;

    public Pagamento(boolean forma, String status) {
        this.forma = forma;
        this.status = status;
    }

    public boolean isForma() {
        return forma;
    }

    public String getStatus() {
        return status;
    }

    public void setForma(boolean forma) {
        this.forma = forma;
    }

    public void setStatus(String status) {
        this.status = status;
    }
        
}
