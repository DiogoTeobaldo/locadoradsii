/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.sql.Date;



/**
 *
 * @author gilbertoalexsantos
 */
public class Cliente extends Pessoa {
    private String status;
    private String idFuncionario;
    
    public Cliente(String nome, String cpf, String telefone, String email, Date data, String cnh, String status){
    	this.nome = nome;
    	this.cpf = cpf;
    	this.telefone = telefone;
    	this.email  =email;
    	this.dataCorreta  = data;
    	this.cnh = cnh;
    	this.status = status;
    	idFuncionario = "";
    }

    public Cliente(){}

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
    	this.status = status;
    }
    
    public String getIdFuncionario() {
    	return idFuncionario;
    }
    
    public void setIdFuncionario(String idFuncionario) {
    	this.idFuncionario = idFuncionario;
    }
    
    
}
