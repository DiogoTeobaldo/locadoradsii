package entity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

/**
 * @author diogoTeobaldo
 */
public class Veiculo {
    private String idVeiculo;
    private String combustivel;
    private String status;
    private float kilometragem;
    private float preco;
    private String cor;
    private String idModelo;
    private String idFuncionario;
    
	public String getIdVeiculo() {
		return idVeiculo;
	}
	public void setIdVeiculo(String idVeiculo) {
		this.idVeiculo = idVeiculo;
	}
	public String getCombustivel() {
		return combustivel;
	}
	public void setCombustivel(String combustivel) {
		this.combustivel = combustivel;
	}
	public String getModelo() {
		return idModelo;
	}
	public void setModelo(String idModelo) {
		this.idModelo = idModelo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public float getKilometragem() {
		return kilometragem;
	}
	public void setKilometragem(float kilometragem) {
		this.kilometragem = kilometragem;
	}
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	
	public float getPreco() {
		return preco;
	}
	public void setPreco(float preco) {
		this.preco = preco;
	}
	
    public String getIdFuncionario() {
    	return idFuncionario;
    }
    
    public void setIdFuncionario(String idFuncionario) {
    	this.idFuncionario = idFuncionario;
    }
	
	public boolean validaPlaca(String placa){
		placa = placa.replace("-", "");
		Pattern pattern = Pattern.compile("[A-Z]{3,3}[0-9]{4,4}");
		Matcher matcher = pattern.matcher(placa);
		
		return matcher.matches();
	}
	
    public boolean validaNome(String nome){
        return nome.matches("[a-zA-Z ]+");
    }
     
    public boolean validaMarca(String marca){
        return marca.matches("[a-zA-Z ]+");
    } 
    
    public boolean validaAno(String ano) {
        return ano.matches("[0-9]{1,8}");
    }
    
    public float getValorPreco(String preco){
    	float valor = 0;
    	try{
    		valor = Float.parseFloat(preco);
    	} catch (Exception e){
    		JOptionPane.showMessageDialog(null, "Valor invalido, o precoo deve " +
    				"ser um valor numerico de pelo menos 1 digito!");
    	}
    	return valor;
    }
    
    public float getValorKilometragem(String km){
    	float valor = 0;
    	try{
    		valor = Float.parseFloat(km);
    	} catch (Exception e){
    		JOptionPane.showMessageDialog(null, "Valor invalido, a Kilometragem deve " +
    				"ser um valor numerico de pelo menos 1 digito!");
    	}
    	return valor;
    }
}