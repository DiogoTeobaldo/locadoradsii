/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.sql.Date;

/**
 *
 * @author gilbertoalexsantos
 */
abstract class Pessoa {
    protected String nome, cpf, telefone, email, data_nasc, cnh;
    
    protected Date dataCorreta;
    
    
    public String getCnh() {
		return cnh;
	}

	public void setCnh(String cnh) {
		this.cnh = cnh;
	}

	public Date getDataCorreta() {
		return dataCorreta;
	}

	public void setDataCorreta(Date dataCorreta) {
		this.dataCorreta = dataCorreta;
	}


    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getEmail() {
        return email;
    }

    public String getData() {
    	if(data_nasc!=null)
        	return data_nasc.substring(6, 10)+data_nasc.substring(2, 6)+data_nasc.substring(0, 2);
        else
        	return null;
    }
    
    public String getDataNormal() {
    	if(data_nasc!=null)
    		return data_nasc.substring(8,10)+data_nasc.substring(4,8)+data_nasc.substring(0,4);
    	else
    		return null;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setData(String data_nasc) {
        this.data_nasc =  data_nasc;
    }
}
