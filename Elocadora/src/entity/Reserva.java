package entity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author gilbertoalexsantos
 */
public class Reserva {
	private String idReserva;
	private String idCliente;
	private String idVeiculo;
    private String data;
    private String hora;
    private String idFuncionario;
    
    public String getData() {
        return data;
    }

    public String getHora() {
        return hora;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }


	public String getidVeiculo() {
		return idVeiculo;
	}


	public String getidCliente() {
		return idCliente;
	}
	
	public void setidVeiculo(String idVeiculo) {
        this.idVeiculo = idVeiculo;
    }
	
	public void setidCliente(String idCliente) {
        this.idCliente = idCliente;
    }
	
	public boolean validaIdCliente(String idCliente) {
		idCliente = idCliente.replace('.', ' ');
		idCliente = idCliente.replace('-', ' ');
		Pattern pattern = Pattern.compile("\\d{3,3}\\s\\d{3,3}\\s\\d{3,3}\\s\\d{2,2}");
		Matcher matcher = pattern.matcher(idCliente);
		
		return matcher.matches();
	}
		
	public boolean validaIdVeiculo(String placa) {
		placa = placa.replace('-', ' ');
		Pattern pattern = Pattern.compile("[A-Z]{3,3}\\s\\d{4,4}");
		Matcher matcher = pattern.matcher(placa);
		
		return matcher.matches();
	}
	
	public boolean validaData(String data) {
		Pattern pattern = Pattern.compile("\\d{2,2}-\\d{2,2}-\\d{4,4}");
		Matcher matcher = pattern.matcher(data);
		boolean valido = true;
		
		if (!matcher.matches())
			return false;
		
		int valor = Integer.parseInt(data.substring(0, 2));
		if (valor < 0 || valor > 31)
			valido = false;
		
		valor = Integer.parseInt(data.substring(3, 5));
		if (valor < 0 || valor > 12)
			valido = false;

		valor = Integer.parseInt(data.substring(6, 10));
		if (valor < 2014)
			valido = false;
		
		return valido;
		
	}
	
	public boolean validaHora(String hora) {
		hora = hora.replaceAll(":", "");
		hora = hora.replaceAll(" ", "");
		Pattern pattern = Pattern.compile("\\d{2,2}\\d{2,2}");
		Matcher matcher = pattern.matcher(hora);
		boolean valido = true;
		
		if (!matcher.matches())
			valido =  false;
		
		int valor = Integer.parseInt(hora.substring(0, 2));
		if (valor < 0 || valor > 23)
			valido = false;
		
		valor = Integer.parseInt(hora.substring(2, 4));
		if (valor < 0 || valor > 59)
			valido = false;
		
		return valido;
	}

	public String getIdReserva() {
		return idReserva;
	}

	public void setIdReserva(String idReserva) {
		this.idReserva = idReserva;
	}

    public String getIdFuncionario() {
    	return idFuncionario;
    }
    
    public void setIdFuncionario(String idFuncionario) {
    	this.idFuncionario = idFuncionario;
    }
}
